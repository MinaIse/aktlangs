package toylangs.mash;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import toylangs.mash.ast.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MashAstTest {
    @Test
    public void test01_num() {
        legal("1;", "Block(ExpressionStatement(IntegerLiteral(1)))");
        legal("2200;", "Block(ExpressionStatement(IntegerLiteral(2200)))");
        illegal("01;");
        illegal("1");
    }

    @Test
    public void test02_str() {
        legal("\"2\";", "Block(ExpressionStatement(StringLiteral(\"2\")))");
        legal("\"Jutukas vanamees\";", "Block(ExpressionStatement(StringLiteral(\"Jutukas vanamees\")))");
        legal("\"uh-oh\";", "Block(ExpressionStatement(StringLiteral(\"uh-oh\")))");
        illegal("\"mkm\"");
        illegal("let a = 'bc';");
        illegal(";");
    }

    @Test
    public void test03_aritm() {
        legal("2+5;", "Block(ExpressionStatement(FunctionCall(\"+\", IntegerLiteral(2), IntegerLiteral(5))))");
        legal("a-1;", "Block(ExpressionStatement(FunctionCall(\"-\", Variable(\"a\"), IntegerLiteral(1))))");
        legal("7*2;", "Block(ExpressionStatement(FunctionCall(\"*\", IntegerLiteral(7), IntegerLiteral(2))))");
        legal("4/(2);", "Block(ExpressionStatement(FunctionCall(\"/\", IntegerLiteral(4), IntegerLiteral(2))))");
        legal("2**5;", "Block(ExpressionStatement(FunctionCall(\"**\", IntegerLiteral(2), IntegerLiteral(5))))");
        legal("1 *2;", "Block(ExpressionStatement(FunctionCall(\"*\", IntegerLiteral(1), IntegerLiteral(2))))");
        legal("(2+7)*5;", "Block(ExpressionStatement(FunctionCall(\"*\", FunctionCall(\"+\", IntegerLiteral(2), IntegerLiteral(7)), IntegerLiteral(5))))");
        legal("-20;", "Block(ExpressionStatement(FunctionCall(\"-\", IntegerLiteral(20))))");
        illegal("a*01");
        illegal("*2;");
    }

    @Test
    public void test04_mushid() {
        legal("[];", "Block(ExpressionStatement(EmptyMush([])))");
        legal("[1,2];", "Block(ExpressionStatement(IntMush([1, 2])))");
        legal("[\"kolm\",\"neli\"];", "Block(ExpressionStatement(StringMush([\"kolm\", \"neli\"])))");
        illegal("[,];");
        illegal("[1,\"kaks\"];");
        illegal("[1,[2]];");
    }

    @Test
    public void test05_muutujad() {
        legal("let x = -12;", "Block(Declaration(\"x\", null, FunctionCall(\"-\", IntegerLiteral(12))))");
        legal("let y = (15-21)/-3;", "Block(Declaration(\"y\", null, FunctionCall(\"/\", FunctionCall(\"-\", IntegerLiteral(15), IntegerLiteral(21)), FunctionCall(\"-\", IntegerLiteral(3)))))");
        legal("let z = \"Mari Juhkental\";", "Block(Declaration(\"z\", null, StringLiteral(\"Mari Juhkental\")))");
        illegal("let;");
        illegal("let 12+3;");
        illegal("let 1 = 2;");
        illegal("12 = 3;");
    }

    @Test
    public void test06_mush_tehted() {
        legal("[1,2] union [3,0,5];", "Block(ExpressionStatement(MushFunction(\"union\", IntMush([1, 2]), IntMush([3, 0, 5]))))");
        legal("[] mash [79,20];", "Block(ExpressionStatement(MushFunction(\"mash\", EmptyMush([]), IntMush([79, 20]))))");
        legal("[] intersect [3,0];", "Block(ExpressionStatement(MushFunction(\"intersect\", EmptyMush([]), IntMush([3, 0]))))");
        legal("x add 7;", "Block(MushAdd(\"x\", IntegerLiteral(7)))");
        legal("x add 7+13;", "Block(MushAdd(\"x\", FunctionCall(\"+\", IntegerLiteral(7), IntegerLiteral(13))))");
        legal("a except e;", "Block(ExpressionStatement(MushFunction(\"except\", Variable(\"a\"), Variable(\"e\"))))");

        illegal("[8,6] add 7;");
        illegal("[1] mash add 2;");
        illegal("union [10,20];");
    }

    @Test
    public void test07_print() {
        legal("print();", "Block(MushPrint(null))");
        legal("print(123);", "Block(MushPrint(IntegerLiteral(123)))");
        legal("print(x);", "Block(MushPrint(Variable(\"x\")))");
        legal("print([42,12,25]);", "Block(MushPrint(IntMush([42, 12, 25])))");

        illegal("print 1;");
        illegal("print{};");
        illegal("PRINT(3)");
        illegal("print(let a = 3;);");
        illegal("print(let b = 1);");
        illegal("print(a add 2;);");
        illegal("print(a add 2);");
    }

    @Test
    public void test08_booleans() {
        legal("true;", "Block(ExpressionStatement(BoolLiteral(true)))");
        legal("true | false;", "Block(ExpressionStatement(BoolBinOp(\"|\", BoolLiteral(true), BoolLiteral(false))))");
        legal("true & false;", "Block(ExpressionStatement(BoolBinOp(\"&\", BoolLiteral(true), BoolLiteral(false))))");
    }

    private void legal(String input, String expectedAstString) {
        MashNode actualAst = MashAst.makeMashAst(input);
        assertEquals(expectedAstString, actualAst.astToString());
    }

    private void illegal(String input) {
        try {
            MashAst.makeMashAst(input);
            fail("Expected parse error: " + input);
        } catch (Exception ignored) {}
    }
}
