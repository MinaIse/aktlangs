package toylangs.mash;

import org.junit.FixMethodOrder;
import org.junit.runners.MethodSorters;
import toylangs.mash.ast.MashNode;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MashCompilerTest {
    public static String lastTestDescription = "";
    public static String successMessage = "kompileeritud programm töötas õigesti!";

    protected static final Path MASH_OUT = Paths.get("aktk-out");

    protected void testProgram(String mashSource, String className, String expectedOutput) {
        Map<String, String> ioPairs = new HashMap<>();
        testProgram(mashSource, className, ioPairs);
    }

    protected void testProgram(String mashSource, String className, Map<String, String> ioPairs) {
        lastTestDescription = "Testing program:\n\n>"
                + mashSource.replaceAll("\r\n", "\n").replaceAll("\n", "\n>");

        try {
            compileProgram(mashSource, className);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void compileProgram(String mashSource, String className) {
        MashNode ast = MashAst.makeMashAst(mashSource);
    }

}
