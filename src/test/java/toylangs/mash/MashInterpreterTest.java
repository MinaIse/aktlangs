package toylangs.mash;

import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.DisableOnDebug;
import org.junit.rules.TestRule;
import org.junit.rules.Timeout;
import org.junit.runners.MethodSorters;
import toylangs.mash.interpreter.MashInterpreter;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class MashInterpreterTest {
    public static String lastTestDescription = "";

    @Rule
    public TestRule globalTimeOut = new DisableOnDebug(new Timeout(500, TimeUnit.MILLISECONDS));

    @Test
    public void test01_simplePrints() {
        check("print();", "\n");
        check("print(3);", "3\n");
        check("print(\"Sõnad ja teod\");", "Sõnad ja teod\n");
        check("print(\"Saabastega\nkass\");", "Saabastega\nkass\n");
        check("print([2,2,3]);", "[2, 2, 3]\n");
        check("print(1==1);", "true\n");
    }

    @Test
    public void test02_variables() {
        check("let a = 42; let b = 38; let c = a + b; print(c);", "80\n");
        check("let a = 10; let b = 5; let a = a + b; print(a);", "15\n");
        check("let m = [17,42,92,1,-15] mash [30]; let n = m except [30,-15,92]; print(n);",
                "[17, 42, 1]\n");
    }

    @Test
    public void test03_operations() {
        check("let s = 44 + \"mm\"; print(s);", "44mm\n");
        check("let s = \"SA\" + 100; print(s);", "SA100\n");
        check("let t = 12; print(t**2);", "144\n");
        check("let m = [10,9,8] mash [7,6]; m add 125 + 6 * -20; print(m);","[10, 9, 8, 7, 6, 5]\n");
    }

    @Test
    public void test04_booleans() {
        check("let bool = 1==1; print(bool);", "true\n");
        check("let bool = 1==2; print(bool);", "false\n");
        check("let bool = true; print(bool);", "true\n");
        check("print(true);", "true\n");
        check("let fal = false; let tru = true; let result = fal | tru; print(result);", "true\n");
        check("let fal = false; let tru = true; let result = fal & tru; print(result);", "false\n");
    }

    private void check(String program, String expectedOutput) {
        lastTestDescription = "Programm: " + monospaceBlock(program);

        PrintStream originalOut = System.out;

        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            System.setOut(new PrintStream(outputStream));

            MashInterpreter.run(program);

            String actualOutput = outputStream.toString().replaceAll("\\r\\n", "\n");
            if (!actualOutput.equals(expectedOutput)) {
                fail("Ootasin väljundit"    + monospaceBlock(expectedOutput)
                        + "\naga tuli"      + monospaceBlock(actualOutput));
            }
        }
        finally {
            System.setOut(originalOut);
        }
    }

    private static String monospaceBlock(String input) {
        return "\n>" + input.replaceAll("\\r\\n", "\n")
                .replaceAll("\\n", "\n>") + "\n";
    }

}
