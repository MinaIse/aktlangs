grammar Mash;
@header { package toylangs.mash; }

// Seda reeglit pole vaja muuta
program: statements EOF;

// Programm koosneb paljudest konstruktsioonidest 
statements
    : (statement ';')+       // Tavalised laused mis peavad lõppema semikooloniga
    ;

statement
    : assignStatement
    | variableDeclaration
    | expression
    | mushAddStatement
    | printStatement
    ;

printStatement
    : 'print(' expression? ')'
    ;

//ifStatement
//    : 'if(' boolExpression ')' ifbody=statement
//        ('elseif(' boolExpression ')' elseifbody=statement)*
//        ('else' elsebody=statement)?
//        'endif'
//    ;
//
//whileStatement
//    : 'while(' boolExpression ')' whilebody=statement 'endif'
//    ;

mushAddStatement
    : Identifier 'add' expression # MushAdd
    ;

assignStatement
    : Identifier '=' expression;

variableDeclaration
    : 'let' VariableName=Identifier ('=' expression)?
    ;

// Võib olla kas mush-, arv-, sõne- või tõeväärtus
expression
    : booleanExpression
    | mushSumExpression
    ;

booleanExpression
    : '!' booleanExpression                             # BoolNot
    | left=booleanExpression op=('&'|'|') right=boolAtom           # BoolBinOp
    | boolAtom                                          # BooleanAtom
    ;

boolAtom
    : Boolean           # BooleanLiteral
    | compareExpression # Comparison
    ;

compareExpression
    : sumExpression ('>'|'<'|'=='|'<>') sumExpression   # BinaryCompare
    | sumExpression                                     # SimpleCompare
    ;

mushSumExpression
    : left=mushSumExpression op=('intersect'|'union'|'except'|'mash') right=mushTermExpression  # BinaryMushSum
    | mushTermExpression                                                                        # SimpleMushSum
    ;

sumExpression
    : sumExpression ('+'|'-') termExpression    # BinarySum
    | termExpression                            # SimpleSum
    ;

mushTermExpression
    : Identifier    # MushVariable
    | Mush          # Mush
    ;

termExpression
    : termExpression ('*'|'/'|'**') factorExpression # BinaryTerm
    | factorExpression                          # SimpleTerm
    ;

factorExpression
    : '-' factorExpression  # UnaryMinus
    | callExpression        # SimpleFactor
    ;

callExpression
    : Identifier '(' (expression (',' expression)*)? ')'    # FunctionCall
    | basicExpression                                       # SimpleCall
    ;

basicExpression
    : Identifier            # Variable
    | Integer               # IntegerLiteral
    | String                # StringLiteral
    | '(' expression ')'    # Parenthesis
    ;

Boolean: 'true'|'false';
Identifier : [a-zA-Z][a-zA-Z0-9]*;
Integer : ('0'|[1-9][0-9]*);
String : '"' ~[\r]* '"';
Mush :'['(('-'?Integer|('-'?Integer(',''-'?Integer)*))|(String|(String(','String)*)))?']';

WS : [ \r\n\t]+ -> skip;