package toylangs.mash;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;

public class Environment<T> {
    /**
     * Sisemised skoobid eespool.
     */
    private final Deque<Map<String, T>> envs = new ArrayDeque<>();

    /**
     * Esialgu peaks olemas olema globaalne skoop, kuhu saab muutujaid deklareerida enne ühtegi skoopi (plokki) sisenemist.
     */
    public Environment() {
        enterBlock();
    }

    /**
     * Deklareerib praeguses skoobis uue muutuja.
     */
    public void declare(String variable) {
        envs.peekFirst().put(variable, null);
    }

    /**
     * Omistab muutujale uue väärtuse kõige sisemises skoobis, kus see muutuja deklareeritud on.
     */
    public void assign(String variable, T value) {
        for (Map<String, T> env : envs) {
            if (env.containsKey(variable)) {
                env.put(variable, value);
                break;
            }
        }
    }

    /**
     * Deklareerib praguses skoobis uue muutuja ja omistab sellele väärtuse.
     */
    public void declareAssign(String variable, T value) {
        envs.peekFirst().put(variable, value);
    }

    /**
     * Tagastab muutuja praeguse väärtuse kõige sisemises skoobis, kus see muutuja deklareeritud on.
     * Deklareerimata või väärtustamata muutujate korral peaks tagastama {@code null}.
     */
    public T get(String variable) {
        for (Map<String, T> env : envs) {
            if (env.containsKey(variable)) {
                return env.get(variable);
            }
        }
        return null;
    }

    /**
     * Tähistab uude skoopi (plokki) sisenemist.
     * Uues skoobis võib üle deklareerida välimiste välimise skoobi muutujaid.
     */
    public void enterBlock() {
        envs.addFirst(new HashMap<>());
    }

    /**
     * Tähistab praegusest skoobist (plokist) väljumist.
     * Unustama peaks kõik sisemises skoobis deklareeritud muutujad.
     */
    public void exitBlock() {
        envs.removeFirst();
    }
}
