package toylangs.mash.compiler;

import toylangs.mash.Environment;
import toylangs.mash.MashVisitor;
import toylangs.mash.ast.*;

public class MashBinding {
    public static void bind(MashNode node) {
        new MashBindingVisitor().visit(node);
    }

    private static class MashBindingVisitor extends MashAstVisitor.VoidVisitor {

        private final Environment<MashNode> env = new Environment<>();

        @Override
        protected void visitVoid(Assignment assignment) {
            assignment.setBinding((VariableBinding) env.get(assignment.getVariableName()));
            visit(assignment.getExpression());
        }

        @Override
        protected void visitVoid(Block block) {
            env.enterBlock();
            for (Statement statement : block.getStatements()) {
                visit(statement);
            }
            env.exitBlock();
        }

        @Override
        protected void visitVoid(BoolBinOp boolBinOp) {
            visit(boolBinOp.getLeft());
            visit(boolBinOp.getRight());
        }

        @Override
        protected void visitVoid(BoolLiteral boolLiteral) {

        }

        @Override
        protected void visitVoid(Declaration declaration) {
            if (declaration.getInitializer() != null)
                visit(declaration.getInitializer());
            env.declareAssign(declaration.getVariableName(), declaration);
        }

        @Override
        protected void visitVoid(EmptyMush emptyMush) {

        }

        @Override
        protected void visitVoid(ExpressionStatement expressionStatement) {
            visit(expressionStatement.getExpression());
        }

        @Override
        protected void visitVoid(FunctionCall functionCall) {
            for (Expression argument : functionCall.getArguments()) {
                visit(argument);
            }
        }

        @Override
        protected void visitVoid(IntegerLiteral integerLiteral) {

        }

        @Override
        protected void visitVoid(IntMush intMush) {

        }

        @Override
        protected void visitVoid(MushAdd mushAdd) {
            visit(mushAdd.getValueToAdd());
        }

        @Override
        protected void visitVoid(MushFunction mushFunction) {
            for (Expression argument : mushFunction.getArguments()) {
                visit(argument);
            }
        }

        @Override
        protected void visitVoid(MushPrint print) {
            visit(print.getExpression());
        }

        @Override
        protected void visitVoid(StringLiteral stringLiteral) {

        }

        @Override
        protected void visitVoid(StringMush stringMush) {

        }

        @Override
        protected void visitVoid(Variable var) {
            var.setBinding((VariableBinding) env.get(var.getVariableName()));
        }
    }
}
