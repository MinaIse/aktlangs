package toylangs.mash.compiler;

import toylangs.mash.ast.*;

import java.util.ArrayList;
import java.util.List;

public class MashTypeChecker {

    public static void check(MashNode ast) {
        MashBinding.bind(ast);
        new MashTypeCheckerVisitor().visit(ast);
    }

    private static final String INTEGER_TYPE    = "Integer";
    private static final String STRING_TYPE     = "String";
    private static final String BOOLEAN_TYPE    = "Boolean";
    private static final String INTMUSH_TYPE    = "IntMush";
    private static final String STRINGMUSH_TYPE = "StringMush";
    private static final String EMPTYMUSH_TYPE  = "EmptyMush";

    private static class MashTypeCheckerVisitor extends MashAstVisitor<String> {
        @Override
        protected String visit(Assignment assignment) {
            if (assignment.getBinding() == null) {
                throw new RuntimeException(String.format("Unbound variable '%s'", assignment.getVariableName()));
            }

            String variableType = assignment.getBinding().getType();
            String expressionType = visit(assignment.getExpression());
            if (!variableType.equals(expressionType)) {
                throw new RuntimeException("Assignment expression type differs from variable type!");
            }

            return null;
        }

        @Override
        protected String visit(Block block) {
            for (Statement statement : block.getStatements()) {
                visit(statement);
            }
            return null;
        }

        @Override
        protected String visit(BoolBinOp boolBinOp) {
            String left = visit(boolBinOp.getLeft());
            String right = visit(boolBinOp.getRight());
            if (left.equals(BOOLEAN_TYPE) && right.equals(BOOLEAN_TYPE))
                return BOOLEAN_TYPE;
            else
                throw new RuntimeException("Invalid types used in expression '"+boolBinOp.astToString()+"': "
                        + "\n\tLeft type  - " + left + "\n\tRight type - " + right);
        }

        @Override
        protected String visit(BoolLiteral boolLiteral) {
            return BOOLEAN_TYPE;
        }

        @Override
        protected String visit(Declaration declaration) {
            String initializerType = visit(declaration.getInitializer());
            if (!isValidType(initializerType))
                throw new RuntimeException(
                        "Unknown type '" + initializerType + "' in declaration initializer expression: "
                                + declaration.getInitializer().astToString());
            return null;
        }

        @Override
        protected String visit(EmptyMush emptyMush) {
            return EMPTYMUSH_TYPE;
        }

        @Override
        protected String visit(ExpressionStatement expressionStatement) {
            visit(expressionStatement.getExpression());
            return null;
        }

        @Override
        protected String visit(FunctionCall functionCall) {
            String name = functionCall.getFunctionName();
            List<String> argTypes = new ArrayList<>();
            for (Expression argument : functionCall.getArguments()) {
                String visit = visit(argument);
                argTypes.add(visit);
            }
            switch (name) {
                case "+":
                    if (argTypes.get(0).equals(STRING_TYPE) || argTypes.get(1).equals(STRING_TYPE))
                        return STRING_TYPE;
                    else if (argTypes.get(0).equals(INTEGER_TYPE) && argTypes.get(1).equals(INTEGER_TYPE))
                        return INTEGER_TYPE;
                    else throw new IllegalArgumentException("Plus types must be either integers or strings!");
                case "==":
                case "!=":
                    if (argTypes.get(0).equals(argTypes.get(1))) return BOOLEAN_TYPE;
                    else throw new IllegalArgumentException("Equality types must match!");
                default:
                    if (functionCall.isArithmeticOperation()) {
                        for (String t : argTypes) {
                            if (!t.equals(INTEGER_TYPE))
                                throw new IllegalArgumentException("Arithmetic operations need integer arguments!");
                        }
                        return INTEGER_TYPE;
                    }
                    else if(functionCall.isComparisonOperation()) {
                        for (String t : argTypes) {
                            if (!t.equals(argTypes.get(0)))
                                throw new IllegalArgumentException(
                                        "Comparison operations need to have arguments of the same type!");
                        }
                    }
                    // CONTINUE AND FINISH!!!!!!1
                    //return null;
                    throw new RuntimeException("Unexpected program state in TypeChecker!");
            }
        }

        @Override
        protected String visit(IntegerLiteral integerLiteral) {
            return INTEGER_TYPE;
        }

        @Override
        protected String visit(IntMush intMush) {
            return INTMUSH_TYPE;
        }

        @Override
        protected String visit(MushAdd mushAdd) {
            String addArgType = visit(mushAdd.getValueToAdd());
            if (!addArgType.equals(INTEGER_TYPE) && !addArgType.equals(STRING_TYPE))
                throw new RuntimeException("Invalid type '"+addArgType+"' for add statement: " + mushAdd.astToString());
            return null;
        }

        @Override
        protected String visit(MushFunction mushFunction) {
            List<Expression> arguments = mushFunction.getArguments();
            String left = visit(arguments.get(0));
            String right = visit(arguments.get(1));
            if (left.equals(right))
                return left;
            throw new RuntimeException("Mushfunction arguments must be the same types of Mush!");
        }

        @Override
        protected String visit(MushPrint print) {
            String type = visit(print.getExpression());
            if (!isValidType(type))
                throw new RuntimeException("Invalid type '"+type+"' for print statement: " + print.astToString());
            return null;
        }

        @Override
        protected String visit(StringLiteral stringLiteral) {
            return null;
        }

        @Override
        protected String visit(StringMush stringMush) {
            return null;
        }

        @Override
        protected String visit(Variable var) {
            if (var.getBinding() == null)
                throw new RuntimeException(String.format("Unbound variable '%s'", var.getVariableName()));
            return var.getBinding().getType();
        }

        private boolean isValidType(String type) {
            return switch (type) {
                case INTEGER_TYPE, STRING_TYPE, BOOLEAN_TYPE, INTMUSH_TYPE, STRINGMUSH_TYPE, EMPTYMUSH_TYPE -> true;
                default -> false;
            };
        }
    }
}
