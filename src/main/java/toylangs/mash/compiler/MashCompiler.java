package toylangs.mash.compiler;

import cma.CMaProgram;
import cma.CMaProgramWriter;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import toylangs.mash.MashAst;
import toylangs.mash.ast.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.objectweb.asm.Opcodes.*;

public class MashCompiler {

    public static void main(String[] args) throws IOException {
        args = new String[] {"src/main/java/toylangs/mash/yks_pluss_yks.mash"};
        if (args.length != 1) {
            throw new IllegalArgumentException("Sellele programmile tuleb anda parameetriks kompileeritava" +
                    " MASH faili nimi!");
        }

        Path sourceFile = Paths.get(args[0]);
        if (!Files.isRegularFile(sourceFile)) {
            throw new IllegalArgumentException("Ei leia faili nimega '"+sourceFile+"'!");
        }

        String className = sourceFile.getFileName().toString().replace(".mash", "");
        Path classFile = sourceFile.toAbsolutePath().getParent().resolve(className + ".class");

        createClassFile(sourceFile, className, classFile);
    }

    private static void createClassFile(Path sourceFile, String className, Path classFile) throws IOException {
        // Loeme faili sisu muutujasse
        String source = Files.readString(sourceFile);

        // Parsime ja moodustame AST'i
        MashNode ast = MashAst.makeMashAst(source);

        // Seome muutujad
        MashBinding.bind(ast);

        // Kompileerime
        byte[] bytes = createClass(ast, className);
        Files.write(classFile, bytes);
    }

    private static byte[] createClass(MashNode ast, String className) {
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_FRAMES);

        // Attribuudid
        cw.visit(V1_8, ACC_PUBLIC + ACC_SUPER, className, null, "java/lang/Object", null);
        cw.visitSource(null, null);

        // main meetod
        MethodVisitor mv = cw.visitMethod(
                ACC_PUBLIC + ACC_STATIC,        // Modifikaatorid
                "main",                         // Meetodi nimi mida loome
                "([Ljava/lang/String;)V",     // Meetodi kirjeldaja
                null,                           // Geneerikute info
                new String[] {"java/io/IOException"});
        mv.visitCode();

        // Terve MASH programm tuleb kompileerida main meetodi sisse
        new MashCompilerVisitor(mv).visit(ast);
        mv.visitInsn(RETURN);
        mv.visitMaxs(0, 0);
        mv.visitEnd();

        // Klassi lõpetamine
        cw.visitEnd();

        // Klassi baidijada genereerimine
        return cw.toByteArray();
    }

    private static class MashCompilerVisitor extends MashAstVisitor.VoidVisitor {
        private final MethodVisitor mv;
        private final Map<VariableBinding, Integer> variableIndices = new HashMap<>();

        private MashCompilerVisitor(MethodVisitor mv) {
            this.mv = mv;
        }

        private int getVariableIndex(VariableBinding binding) {
            // Kasuta teadaolevat indeksit, kui see on olemas, või leia järgmine vaba indeks ja salvesta see.
            // Esimene muutja saab indeksi 1, sest indeksil 0 on main meetodi parameeter (String[] args)
            return variableIndices.computeIfAbsent(binding, ignoreBinding -> variableIndices.size() + 1);
        }

        @Override
        protected void visitVoid(Assignment assignment) {

        }

        @Override
        protected void visitVoid(Block block) {

        }

        @Override
        protected void visitVoid(BoolBinOp boolBinOp) {

        }

        @Override
        protected void visitVoid(BoolLiteral boolLiteral) {

        }

        @Override
        protected void visitVoid(Declaration declaration) {

        }

        @Override
        protected void visitVoid(EmptyMush emptyMush) {

        }

        @Override
        protected void visitVoid(ExpressionStatement expressionStatement) {

        }

        @Override
        protected void visitVoid(FunctionCall functionCall) {

        }

        @Override
        protected void visitVoid(IntegerLiteral integerLiteral) {

        }

        @Override
        protected void visitVoid(IntMush intMush) {

        }

        @Override
        protected void visitVoid(MushAdd mushAdd) {

        }

        @Override
        protected void visitVoid(MushFunction mushFunction) {

        }

        @Override
        protected void visitVoid(MushPrint print) {

        }

        @Override
        protected void visitVoid(StringLiteral stringLiteral) {

        }

        @Override
        protected void visitVoid(StringMush stringMush) {

        }

        @Override
        protected void visitVoid(Variable var) {

        }
    }
}
