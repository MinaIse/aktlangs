package toylangs.mash.interpreter;

import toylangs.mash.ast.*;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class MashInterpreterBuiltins {
    //private static BufferedReader inputReader = null;

    // sisend-väljund
    public static void print(String value) {
        System.out.println(value);
    }

    public static void print(Integer value) {
        System.out.println(value);
    }

    public static void print(Mush<? extends Expression> mush) {
        System.out.println(mush.getValues().toString());
    }

    public static void print(Object o) {
        if (o == null) System.out.println();
        System.out.println(o);
    }

    protected static boolean canCastToIntMush(Object leftVisit, Object rightVisit) {
        return      leftVisit instanceof IntMush && rightVisit instanceof IntMush
                || (leftVisit instanceof IntMush   && rightVisit instanceof EmptyMush
                ||  leftVisit instanceof EmptyMush && rightVisit instanceof IntMush);
    }

    protected static boolean canCastToStringMush(Object leftVisit, Object rightVisit) {
        return leftVisit instanceof StringMush && rightVisit instanceof StringMush
                || (leftVisit instanceof StringMush && rightVisit instanceof EmptyMush
                || leftVisit instanceof EmptyMush && rightVisit instanceof StringMush);
    }

    protected static IntMush applyFunction(String functionName, IntMush left, IntMush right) {
        List<Integer> values = new ArrayList<>();
        List<Integer> lVals = left.getValues();
        List<Integer> rVals = right.getValues();
        switch (functionName) {
            case "intersect":
                for (Integer e : lVals) {
                    if (rVals.contains(e) && !values.contains(e))
                        values.add(e);
                }
                break;
            case "union":
                for (Integer e : lVals) {
                    if (!values.contains(e))
                        values.add(e);
                }
                for (Integer e : rVals) {
                    if (!values.contains(e))
                        values.add(e);
                }
                break;
            case "except":
                for (Integer e : lVals) {
                    if (!rVals.contains(e) && !values.contains(e))
                        values.add(e);
                }
                break;
            case "mash":
                values.addAll(lVals);
                values.addAll(rVals);
        }
        return new IntMush(values);
    }

    protected static StringMush applyFunction(String functionName, StringMush left, StringMush right) {
        List<String> values = new ArrayList<>();
        List<String> lVals = left.getValues();
        List<String> rVals = right.getValues();
        switch (functionName) {
            case "intersect":
                for (String e : lVals) {
                    if (rVals.contains(e) && !values.contains(e))
                        values.add(e);
                }
                break;
            case "union":
                for (String e : lVals) {
                    if (!values.contains(e))
                        values.add(e);
                }
                for (String e : rVals) {
                    if (!values.contains(e))
                        values.add(e);
                }
                break;
            case "except":
                for (String e : lVals) {
                    if (!rVals.contains(e) && !values.contains(e))
                        values.add(e);
                }
        }
        return new StringMush(values);
    }

    /**
     * Returns whether the first argument is greater compared to the second argument.
     * <p>
     * If arguments are Strings, compares them lexicographically. If arguments are Integers, compares them numerically.
     *
     * @throws RuntimeException If different types are compared or if the types are not 'String' or 'Integer', an exception is thrown.
     * @param leftVisit     The left expression as an {@code Object}
     * @param rightVisit    The right expression as an {@code Object}
     * @return {@code boolean}
     */
    protected static boolean greater(Object leftVisit, Object rightVisit) {
        if (leftVisit instanceof String && rightVisit instanceof String) {
            return ((String)leftVisit).compareTo((String)rightVisit) > 0;
        }
        else if (leftVisit instanceof Integer && rightVisit instanceof Integer) {
            return ((Integer)leftVisit) > ((Integer)rightVisit);
        }
        else throw new RuntimeException(
                    "Inappropriate arguments for '>' operator:\n\t - " + leftVisit
                            + "\n\t - " + rightVisit);
    }

    /**
     * Returns whether the first argument is less compared to the second argument.
     * <p>
     * If arguments are Strings, compares them lexicographically. If arguments are Integers, compares them numerically.
     *
     * @throws RuntimeException If different types are compared or if the types are not 'String' or 'Integer', an exception is thrown.
     * @param leftVisit     The left expression as an {@code Object}
     * @param rightVisit    The right expression as an {@code Object}
     * @return {@code boolean}
     */
    protected static boolean less(Object leftVisit, Object rightVisit) {
        if (leftVisit instanceof String && rightVisit instanceof String) {
            return ((String)leftVisit).compareTo((String)rightVisit) < 0;
        }
        else if (leftVisit instanceof Integer && rightVisit instanceof Integer) {
            return ((Integer)leftVisit) < ((Integer)rightVisit);
        }
        else throw new RuntimeException(
                    "Inappropriate arguments for '<' operator:\n\t - " + leftVisit
                            + "\n\t - " + rightVisit);
    }

    /**
     * Returns whether the first argument is equal to the second argument.
     * <p>
     * The arguments are cast to a mutual type and then compared.
     * The expected argument types are {@code Integer}, {@code String} and {@code Mush}. If the types are the same, but different from the mentioned types, they are compared as {@code Objects}.
     *
     * @throws RuntimeException If different types are compared, an exception is thrown.
     * @param leftVisit     The left expression as an {@code Object}
     * @param rightVisit    The right expression as an {@code Object}
     * @return {@code boolean}
     */
    protected static boolean equal(Object leftVisit, Object rightVisit) {
        // Since String and Integer both override equals() method, no casting is required.
        if (leftVisit instanceof String && rightVisit instanceof String
                || leftVisit instanceof Integer && rightVisit instanceof Integer)
            return leftVisit.equals(rightVisit);
        else if (leftVisit instanceof IntMush && rightVisit instanceof IntMush) {
            IntMush lmush = (IntMush) leftVisit;
            IntMush rmush = (IntMush) rightVisit;
            List<Integer> lVals = lmush.getValues();
            List<Integer> rVals = rmush.getValues();
            if (lVals.size() == rVals.size()) {
                for (int i = 0; i < lVals.size(); i++) {
                    if (!lVals.get(i).equals(rVals.get(i))) return false;
                }
                return true;
            }
        }
        throw new RuntimeException(
                "Invalid Objects passed to MashInterpreter.equal() method:" +
                        "\n\t - " + leftVisit + "\n\t - " + rightVisit);
    }

    /**
     * Returns whether the first argument is not equal to the second argument.
     * <p>
     *     This method was created because apparently you can't get a boolean from the statement '!equal()',
     *     when 'equal()' is a static method.
     * </p>
     * The arguments are cast to a mutual type and then compared.
     * The expected argument types are {@code Integer}, {@code String} and {@code Mush}.
     * If the types are the same, but different from the mentioned types, they are compared as {@code Objects}.
     *
     * @throws RuntimeException If different types are compared, an exception is thrown.
     * @param leftVisit     The left expression as an {@code Object}
     * @param rightVisit    The right expression as an {@code Object}
     * @return {@code boolean}
     */
    protected static boolean notEqual(Object leftVisit, Object rightVisit) {
        // Since String and Integer both override equals() method, no casting is required.
        if (leftVisit instanceof String && rightVisit instanceof String
                || leftVisit instanceof Integer && rightVisit instanceof Integer)
            return !leftVisit.equals(rightVisit);
        else if (leftVisit instanceof IntMush && rightVisit instanceof IntMush) {
            IntMush lmush = (IntMush) leftVisit;
            IntMush rmush = (IntMush) rightVisit;
            List<Integer> lVals = lmush.getValues();
            List<Integer> rVals = rmush.getValues();
            if (lVals.size() == rVals.size()) {
                for (int i = 0; i < lVals.size(); i++) {
                    if (!lVals.get(i).equals(rVals.get(i))) return true;
                }
                return false;
            } else return true;
        }
        throw new RuntimeException(
                "Invalid Objects passed to MashInterpreter.equal() method:" +
                        "\n\t - " + leftVisit + "\n\t - " + rightVisit);
    }

    /**
     * Adds two Integers or Strings and returns the sum.
     * <p>
     * If one of the arguments is of type {@code String}, the return value will also be a concatenated String.
     * If both arguments are integers, the returned value will be their sum.
     * Other types are not allowed and will result in an exception.
     * @throws RuntimeException if the arguments' types differ from String and/or Integer.
     * @param leftVisit     {@code Object} String or Integer literal.
     * @param rightVisit    {@code Object} String or Integer literal.
     * @return arguments' sum.
     */
    protected static Object add(Object leftVisit, Object rightVisit) {
        if (leftVisit instanceof String && rightVisit instanceof String) {
            return ((String) leftVisit).concat((String) rightVisit);
        } else if (leftVisit instanceof String && rightVisit instanceof Integer) {
            return ((String) leftVisit).concat(((Integer)rightVisit).toString());
        }  else if (leftVisit instanceof Integer && rightVisit instanceof String) {
            return (((Integer)leftVisit).toString()).concat((String) rightVisit);
        } else if (leftVisit instanceof Integer && rightVisit instanceof Integer) {
            return ((Integer) leftVisit) + ((Integer) rightVisit);
        } else throw new RuntimeException(
                "Invalid arguments passed to MashInterpreter add():" +
                        "\n\t - " + leftVisit + "\n\t - " + rightVisit);
    }

    /**
     * Treats arguments as {@code Integer} types and performs the appropriate arithmetic operation.
     * <p></>
     * If arguments are not of type {@code Integer}, then an exception is thrown,
     * since these arithmetic operations can only be used with numeric literals in Mash.
     * @param op            {@code String} the sign of an arithmetic operation from the set {'-','*','/','**'}
     * @param leftVisit     {@code Object} first Integer literal.
     * @param rightVisit    {@code Object} second Integer literal.
     * @return Product of specified operand.
     */
    protected static Object intOp(String op, Object leftVisit, Object rightVisit) {
        if (leftVisit instanceof Integer && rightVisit instanceof Integer) {
            Integer left = (Integer) leftVisit;
            Integer right = (Integer) rightVisit;
            return switch (op) {
                case "-"  -> left - right;
                case "*"  -> left * right;
                case "/"  -> left / right;
                case "**" -> pow(left, right);
                default   -> throw new RuntimeException(
                        "Invalid operand passed to method intOp(): '" + op + "'!");
            };
        }
        throw new RuntimeException("Only numeric literals are allowed here! Arguments received:" +
                "\n\t - " + leftVisit + "\n\t - " + rightVisit);
    }

    protected static Integer pow(Integer left, Integer right) {
        if (right < 0) throw new RuntimeException(
                "Illegal arguments to MashInterpreter.pow():" +
                        "\n\t - " + left + "\n\t - " + right +
                        "Powers less than 0 are not allowed! ");
        int n = right-1;
        if (n == 0) return left;
        return left * pow(left, n);
    }
}
