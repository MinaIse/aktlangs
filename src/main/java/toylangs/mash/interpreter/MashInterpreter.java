package toylangs.mash.interpreter;

import toylangs.mash.Environment;
import toylangs.mash.MashAst;
import toylangs.mash.ast.*;

import java.util.List;

public class MashInterpreter {

    public static void run(String program) {
        MashNode ast = MashAst.makeMashAst(program);
        new AktkInterpreterVisitor().visit(ast);
    }

    private static class AktkInterpreterVisitor extends MashAstVisitor<Object> {
        private final Environment<Object> env = new Environment<>();



        @Override
        protected Object visit(Assignment assignment) {
            env.assign(assignment.getVariableName(), visit(assignment.getExpression()));
            return null;
        }

        @Override
        protected Object visit(Block mashProg) {
            env.enterBlock();
            for (Statement statement : mashProg.getStatements()) {
                visit(statement);
            }
            env.exitBlock();
            return null;
        }

        @Override
        protected Object visit(BoolBinOp boolBinOp) {
            String op = boolBinOp.getOp();
            Object left = visit(boolBinOp.getLeft());
            Object right = visit(boolBinOp.getRight());
            return switch (op) {
                case "&" -> ((boolean) left) && ((boolean) right);
                case "|" -> ((boolean) left) || ((boolean) right);
                default  -> throw new RuntimeException("Unexpected program state in MashInterpreter!" +
                        "\nBoolBinOp visited:\n\tOp -\t" + op +
                        "\n\tLeft -\t" + left +
                        "\n\tRight -\t" + right);
            };
        }

        @Override
        protected Object visit(BoolLiteral boolLiteral) {
            return boolLiteral.getValue();
        }

        @Override
        protected Object visit(Declaration declaration) {
            String variableName = declaration.getVariableName();
            Expression initializer = declaration.getInitializer();
            Object exprVisit = null;
            if (initializer instanceof IntMush)
                exprVisit = visit((IntMush) initializer);
            else if (initializer instanceof StringMush)
                exprVisit = visit((StringMush) initializer);
            else
                exprVisit = visit(initializer);
            env.declareAssign(variableName, exprVisit);
            return null;
        }

        @Override
        protected Object visit(EmptyMush emptyMush) {
            return emptyMush.getValues();
        }

        @Override
        protected Object visit(ExpressionStatement expressionStatement) {
            return visit(expressionStatement.getExpression());
        }

        @Override
        protected Object visit(FunctionCall functionCall) {
            String functionName = functionCall.getFunctionName();
            List<Expression> arguments = functionCall.getArguments();
            Object leftVisit = visit(arguments.get(0));
            if (functionName.equals("-") && arguments.size() == 1)
                return -((Integer) leftVisit);
            Object rightVisit = visit(arguments.get(1));
            if (functionCall.isComparisonOperation()) {
                return switch (functionName) {
                    case ">"  -> MashInterpreterBuiltins.greater(leftVisit, rightVisit);
                    case "<"  -> MashInterpreterBuiltins.less(leftVisit, rightVisit);
                    case "==" -> MashInterpreterBuiltins.equal(leftVisit, rightVisit);
                    case "<>" -> MashInterpreterBuiltins.notEqual(leftVisit, rightVisit);
                    default   -> throw new IllegalStateException("Unexpected value: " + functionName);
                };
            } else if (functionCall.isArithmeticOperation()) {
                if (functionName.equals("+"))
                    return MashInterpreterBuiltins.add(leftVisit, rightVisit);
                return MashInterpreterBuiltins.intOp(functionName, leftVisit, rightVisit);
            }
            return null;
        }

        @Override
        protected Object visit(IntegerLiteral num) {
            return num.getValue();
        }

        @Override
        protected Object visit(IntMush intMush) {
            return intMush;
        }

        @Override
        protected Object visit(MushAdd mushAdd) {
            Object mushObject = getVariable(mushAdd.getVariableName());
            Object valueObject = visit(mushAdd.getValueToAdd());
            Mush mush = null;
            if (mushObject instanceof IntMush) {
                mush = (IntMush) mushObject;
                Integer value = null;
                if (valueObject instanceof Integer) {
                    value = (Integer) valueObject;
                    mush.add(value);
                } else
                    throw new RuntimeException("Invalid value to add to an IntMush!");
            } else if (mushObject instanceof StringMush) {
                mush = (StringMush) mushObject;
                String value = null;
                if (valueObject instanceof String) {
                    value = (String) valueObject;
                    mush.add(value);
                } else
                    throw new RuntimeException("Invalid value to add to a StringMush!");
            }
            return null;
        }

        private Object getVariable(String variableName) {
            Object varValue = env.get(variableName);
            if (varValue != null)
                return varValue;
            else
                throw new RuntimeException("No such variable defined in visible scopes: '" + variableName + "'!");
        }

        @Override
        protected Object visit(MushFunction mushFunction) {
            String functionName = mushFunction.getFunctionName();
            List<Expression> arguments = mushFunction.getArguments();
            Object leftVisit  = visit(arguments.get(0));
            Object rightVisit = visit(arguments.get(1));
            if (MashInterpreterBuiltins.canCastToIntMush(leftVisit, rightVisit)) {
                IntMush left  = (IntMush) leftVisit;
                IntMush right = (IntMush) rightVisit;
                return MashInterpreterBuiltins.applyFunction(functionName, left, right);
            } else if (MashInterpreterBuiltins.canCastToStringMush(leftVisit, rightVisit)) {
                StringMush left  = (StringMush) leftVisit;
                StringMush right = (StringMush) rightVisit;
                return MashInterpreterBuiltins.applyFunction(functionName, left, right);
            } else {
                throw new RuntimeException(
                        "\nIncompatible mush instances passed to MushFunction '"
                                + functionName + ":\n\t - " + leftVisit + "\n\t - " + rightVisit);
            }
        }

        @Override
        protected Object visit(MushPrint print) {
            Expression expression = print.getExpression();
            // Kui prinditav avaldis on tühi, nt 'print();',
            // siis ei saa teda visitida!
            Object exprVisit = null;
            if (expression != null) {
                exprVisit = visit(expression);
            }
            if (exprVisit instanceof String)
                System.out.println((String) exprVisit);
            else if (exprVisit instanceof Integer)
                System.out.println((Integer) exprVisit);
            else if (exprVisit instanceof Mush)
                System.out.println(((Mush) exprVisit).getValues().toString());
            else if (exprVisit instanceof Boolean)
                System.out.println(exprVisit);
            else if (exprVisit == null)
                System.out.println();
            else
                throw new RuntimeException("Invalid Expression passed to print statement: " + exprVisit);
            return null;
        }

        @Override
        protected Object visit(StringLiteral str) {
            return str.getValue();
        }

        @Override
        protected Object visit(StringMush stringMush) {
            return stringMush;
        }

        @Override
        protected Object visit(Variable var) {
            return getVariable(var.getVariableName());
        }
    }
}

