package toylangs.mash.ast;

import com.google.errorprone.annotations.Var;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Declaration extends Statement implements VariableBinding {
    private final String variableName;
    private final Expression initializer;
    private String type;

    public Declaration(String variableName, Expression initializer) {
        this.variableName = variableName;
        this.initializer = initializer;
        this.type = null;
    }

    public String getVariableName() {
        return variableName;
    }

    @Override
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Expression getInitializer() {
        return initializer;
    }

    @Override
    protected List<Object> getChildren() {
        return Arrays.asList(variableName, type, initializer);
    }

    @Override
    public <T> T accept(MashAstVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
