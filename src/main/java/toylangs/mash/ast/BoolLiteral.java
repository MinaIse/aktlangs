package toylangs.mash.ast;

import java.util.Collections;
import java.util.List;

public class BoolLiteral extends Expression {
    private final boolean value;

    public BoolLiteral(boolean value) {
        this.value = value;
    }

    public boolean getValue() {
        return value;
    }

    @Override
    protected List<Object> getChildren() {
        return Collections.singletonList(value);
    }

    @Override
    public <T> T accept(MashAstVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
