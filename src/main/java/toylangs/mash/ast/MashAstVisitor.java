package toylangs.mash.ast;

public abstract class MashAstVisitor<T> {
    protected abstract T visit(Assignment assignment);
    protected abstract T visit(Block block);
    protected abstract T visit(BoolBinOp boolBinOp);
    protected abstract T visit(BoolLiteral boolLiteral);
    protected abstract T visit(Declaration declaration);
    protected abstract T visit(EmptyMush emptyMush);
    protected abstract T visit(ExpressionStatement expressionStatement);
    protected abstract T visit(FunctionCall functionCall);
    protected abstract T visit(IntegerLiteral integerLiteral);
    protected abstract T visit(IntMush intMush);
    protected abstract T visit(MushAdd mushAdd);
    protected abstract T visit(MushFunction mushFunction);
    protected abstract T visit(MushPrint print);
    protected abstract T visit(StringLiteral stringLiteral);
    protected abstract T visit(StringMush stringMush);
    protected abstract T visit(Variable var);

    public final T visit(MashNode node) {
        return node.accept(this);
    }

    public static abstract class VoidVisitor extends MashAstVisitor<Void> {

        protected abstract void visitVoid(Assignment assignment);
        protected abstract void visitVoid(Block block);
        protected abstract void visitVoid(BoolBinOp boolBinOp);
        protected abstract void visitVoid(BoolLiteral boolLiteral);
        protected abstract void visitVoid(Declaration declaration);
        protected abstract void visitVoid(EmptyMush emptyMush);
        protected abstract void visitVoid(ExpressionStatement expressionStatement);
        protected abstract void visitVoid(FunctionCall functionCall);
        protected abstract void visitVoid(IntegerLiteral integerLiteral);
        protected abstract void visitVoid(IntMush intMush);
        protected abstract void visitVoid(MushAdd mushAdd);
        protected abstract void visitVoid(MushFunction mushFunction);
        protected abstract void visitVoid(MushPrint print);
        protected abstract void visitVoid(StringLiteral stringLiteral);
        protected abstract void visitVoid(StringMush stringMush);
        protected abstract void visitVoid(Variable var);

        @Override
        protected final Void visit(Assignment assignment) {
            visitVoid(assignment);
            return null;
        }

        @Override
        protected final Void visit(BoolBinOp boolBinOp) {
            visitVoid(boolBinOp);
            return null;
        }

        @Override
        protected final Void visit(Block block) {
            visitVoid(block);
            return null;
        }

        @Override
        protected final Void visit(BoolLiteral boolLiteral) {
            visitVoid(boolLiteral);
            return null;
        }

        @Override
        protected final Void visit(Declaration declaration) {
            visitVoid(declaration);
            return null;
        }

        @Override
        protected final Void visit(EmptyMush emptyMush) {
            visitVoid(emptyMush);
            return null;
        }

        @Override
        protected final Void visit(ExpressionStatement expressionStatement) {
            visitVoid(expressionStatement);
            return null;
        }

        @Override
        protected final Void visit(FunctionCall functionCall) {
            visitVoid(functionCall);
            return null;
        }

        @Override
        protected final Void visit(IntegerLiteral integerLiteral) {
            visitVoid(integerLiteral);
            return null;
        }

        @Override
        protected final Void visit(IntMush intMush) {
            visitVoid(intMush);
            return null;
        }

        @Override
        protected final Void visit(MushAdd mushAdd) {
            visitVoid(mushAdd);
            return null;
        }

        @Override
        protected final Void visit(MushFunction mushFunction) {
            visitVoid(mushFunction);
            return null;
        }

        @Override
        protected final Void visit(MushPrint mushPrint) {
            visitVoid(mushPrint);
            return null;
        }

        @Override
        protected final Void visit(StringLiteral stringLiteral) {
            visitVoid(stringLiteral);
            return null;
        }

        @Override
        protected final Void visit(StringMush stringMush) {
            visitVoid(stringMush);
            return null;
        }

        @Override
        protected final Void visit(Variable variable) {
            visitVoid(variable);
            return null;
        }
    }
}
