package toylangs.mash.ast;

import java.util.List;

public class StringMush extends Mush<String> {
    public StringMush(List<String> values) {
        super(values);
    }

    @Override
    public List<String> getValues() {
        return this.values;
    }

    @Override
    public void add(Integer value) {
        throw new RuntimeException("Mush element type mismatch! Integer values can not be added to String mushes!!!");
    }

    @Override
    public void add(String value) {
        this.values.add(value);
    }

    @Override
    public <T> T accept(MashAstVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
