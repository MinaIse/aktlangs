package toylangs.mash.ast;

import java.util.Arrays;
import java.util.List;

public class MushAdd extends Statement {
    private final String variableName;
    private final Expression valueToAdd;

    public MushAdd(String variableName, Expression valueToAdd) {
        this.variableName = variableName;
        this.valueToAdd = valueToAdd;
    }

    public String getVariableName() {
        return variableName;
    }

    public Expression getValueToAdd() {
        return valueToAdd;
    }

    @Override
    protected List<Object> getChildren() {
        return Arrays.asList(variableName, valueToAdd);
    }

    @Override
    public <T> T accept(MashAstVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
