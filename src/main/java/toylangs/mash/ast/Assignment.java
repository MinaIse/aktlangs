package toylangs.mash.ast;

import java.util.Arrays;
import java.util.List;

public class Assignment extends Statement {
    private final String variableName;
    private final Expression expression;

    private VariableBinding binding = null;

    public Assignment(String variableName, Expression expression) {
        this.variableName = variableName;
        this.expression = expression;
    }

    public String getVariableName() {
        return variableName;
    }

    public Expression getExpression() {
        return expression;
    }

    public VariableBinding getBinding() {
        return binding;
    }

    public void setBinding(VariableBinding binding) {
        this.binding = binding;
    }

    @Override
    protected List<Object> getChildren() {
        return Arrays.asList(variableName, expression);
    }

    @Override
    public <T> T accept(MashAstVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
