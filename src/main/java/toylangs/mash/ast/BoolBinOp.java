package toylangs.mash.ast;

import java.util.Arrays;
import java.util.List;

public class BoolBinOp extends Expression {
    private final String op;
    private final Expression left;
    private final Expression right;

    public BoolBinOp(String op, Expression left, Expression right) {
        this.op = op;
        this.left = left;
        this.right = right;
    }

    public String getOp() {
        return op;
    }

    public Expression getLeft() {
        return left;
    }

    public Expression getRight() {
        return right;
    }

    @Override
    protected List<Object> getChildren() {
        return Arrays.asList(op, left, right);
    }

    @Override
    public <T> T accept(MashAstVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
