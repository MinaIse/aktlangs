package toylangs.mash.ast;

public interface VariableBinding {
    String getVariableName();
    String getType();
}
