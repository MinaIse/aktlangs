package toylangs.mash.ast;

import java.util.Collections;
import java.util.List;

public abstract class Mush<R> extends Expression {
    protected final List<R> values;

    public Mush(List<R> values) {
        this.values = values;
    }

    public abstract List<R> getValues();

    public abstract void add(Integer value);

    public abstract void add(String value);

    @Override
    protected List<Object> getChildren() {
        return Collections.singletonList(values);
    }
}
