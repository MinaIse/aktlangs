package toylangs.mash.ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class EmptyMush extends Mush<Void> {
    public EmptyMush() {
        super(new ArrayList<>());
    }

    @Override
    public List<Void> getValues() {
        return Collections.emptyList();
    }

    @Override
    public void add(Integer value) {}

    @Override
    public void add(String value) {}

    @Override
    public <T> T accept(MashAstVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
