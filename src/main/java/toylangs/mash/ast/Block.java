package toylangs.mash.ast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Block extends Statement {
    private final List<Statement> statements;

    public Block(List<Statement> statements) {
        this.statements = statements;
    }

    public List<Statement> getStatements() {
        return statements;
    }

    @Override
    protected List<Object> getChildren() {
        return new ArrayList<>(statements);
    }

    @Override
    public <T> T accept(MashAstVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
