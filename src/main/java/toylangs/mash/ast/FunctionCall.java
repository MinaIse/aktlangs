package toylangs.mash.ast;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class FunctionCall extends Expression {
    private final String functionName;
    private final List<Expression> arguments;

    public FunctionCall(String functionName, List<Expression> arguments) {
        this.functionName = functionName;
        this.arguments = arguments;
    }

    public FunctionCall(String functionName, Expression... arguments) {
        this(functionName, Arrays.asList(arguments));
    }

    public String getFunctionName() {
        return functionName;
    }

    public List<Expression> getArguments() {
        return Collections.unmodifiableList(arguments);
    }

    @Override
    protected List<Object> getChildren() {
        List<Object> children = new ArrayList<>();
        children.add(functionName);
        children.addAll(arguments);
        return children;
    }

    public boolean isComparisonOperation() {
        return Arrays.asList(">", "<", "==", "<>").contains(functionName);
    }

    public boolean isArithmeticOperation() {
        return Arrays.asList("+", "-", "*", "/", "**").contains(functionName);
    }

    @Override
    public <T> T accept(MashAstVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
