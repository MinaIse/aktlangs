package toylangs.mash.ast;

import java.util.Collections;
import java.util.List;

public class MushPrint extends Statement {
    private final Expression expression;

    public MushPrint(Expression expression) {
        this.expression = expression;
    }

    public Expression getExpression() {
        return expression;
    }

    @Override
    protected List<Object> getChildren() {
        return Collections.singletonList(expression);
    }

    @Override
    public <T> T accept(MashAstVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
