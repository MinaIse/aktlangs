package toylangs.mash.ast;

import java.util.ArrayList;
import java.util.List;

public class MushFunction extends Expression {
    private final String functionName;
    private final List<Expression> arguments;

    public MushFunction(String functionName, List<Expression> arguments) {
        this.functionName = functionName;
        this.arguments = arguments;
    }

    public String getFunctionName() {
        return functionName;
    }

    public List<Expression> getArguments() {
        return arguments;
    }

    @Override
    protected List<Object> getChildren() {
        List<Object> children = new ArrayList<>();
        children.add(functionName);
        children.addAll(arguments);
        return children;
    }

    @Override
    public <T> T accept(MashAstVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
