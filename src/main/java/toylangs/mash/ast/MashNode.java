package toylangs.mash.ast;

import org.apache.commons.text.StringEscapeUtils;
import toylangs.AbstractNode;

import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

public abstract class MashNode extends AbstractNode {
    protected abstract List<Object> getChildren();
    public abstract <T> T accept(MashAstVisitor<T> visitor);

    public String astToString() {
        StringJoiner joiner = new StringJoiner(", ", this.getClass().getSimpleName() + "(", ")");
        for (Object child : getChildren()) {
            String childString;
            if (child instanceof String)
                childString = "\"" + StringEscapeUtils.escapeJava((String) child) + "\"";
            else {
                if (child instanceof Declaration)
                    childString = ((Declaration) child).astToString();
                else if (child instanceof BoolBinOp)
                    childString = ((BoolBinOp) child).astToString();
                else if (child instanceof BoolLiteral)
                    childString = ((BoolLiteral) child).astToString();
                else if (child instanceof IntegerLiteral)
                    childString = ((IntegerLiteral) child).astToString();
                else if (child instanceof StringLiteral)
                    childString = ((StringLiteral) child).astToString();
                else if (child instanceof Variable)
                    childString = ((Variable) child).astToString();
                else if (child instanceof Mush) {
                    if (child instanceof EmptyMush)
                        childString = "EmptyMush([])";
                    else if (child instanceof StringMush)
                        childString = ((StringMush) child).astToString();
                    else if (child instanceof IntMush)
                        childString = ((IntMush) child).astToString();
                    else
                        childString = Objects.toString(child);
                }
                else if (child instanceof FunctionCall)
                    childString = ((FunctionCall) child).astToString();
                else if (child instanceof MushFunction)
                    childString = ((MushFunction) child).astToString();
                else if (child instanceof Statement)
                    childString = ((Statement) child).astToString();
                else
                    childString = Objects.toString(child); // child may be null
            }
            joiner.add(childString);
        }
        return joiner.toString();
    }
}