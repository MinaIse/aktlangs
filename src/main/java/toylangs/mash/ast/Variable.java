package toylangs.mash.ast;

import java.util.Collections;
import java.util.List;

public class Variable extends Expression {
    private final String variableName;

    private VariableBinding binding = null;

    public Variable(String variableName) {
        this.variableName = variableName;
    }

    public String getVariableName() {
        return variableName;
    }

    public VariableBinding getBinding() {
        return binding;
    }

    public void setBinding(VariableBinding binding) {
        this.binding = binding;
    }

    @Override
    protected List<Object> getChildren() {
        return Collections.singletonList(variableName);
    }

    @Override
    public <T> T accept(MashAstVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
