package toylangs.mash.ast;

import java.util.List;

public class IntMush extends Mush<Integer> {
    public IntMush(List<Integer> values) {
        super(values);
    }

    @Override
    public List<Integer> getValues() {
        return this.values;
    }

    @Override
    public void add(Integer value) {
        this.values.add(value);
    }

    @Override
    public void add(String value) {
        throw new RuntimeException("Mush element type mismatch! String values can not be added to Integer mushes!!!");
    }

    @Override
    public <T> T accept(MashAstVisitor<T> visitor) {
        return visitor.visit(this);
    }
}
