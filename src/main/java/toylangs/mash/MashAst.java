package toylangs.mash;

import org.antlr.v4.runtime.BailErrorStrategy;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ParseTree;
import toylangs.mash.ast.*;
import utils.ExceptionErrorListener;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MashAst {

    public static MashNode makeMashAst(String input) {
        MashLexer lexer = new MashLexer(CharStreams.fromString(input));
        lexer.removeErrorListeners();
        lexer.addErrorListener(new ExceptionErrorListener());

        MashParser parser = new MashParser(new CommonTokenStream(lexer));
        parser.removeErrorListeners();
        parser.setErrorHandler(new BailErrorStrategy());

        ParseTree tree = parser.program();
        return parseTreeToAst(tree);
    }

    private static MashNode parseTreeToAst(ParseTree tree) {
        MashAstStatementVisitor statementVisitor = new MashAstStatementVisitor();
        return statementVisitor.visit(tree);
    }

    private static class MashAstExpressionVisitor extends MashBaseVisitor<Expression> {

        @Override
        public Expression visitBooleanLiteral(MashParser.BooleanLiteralContext ctx) {
            String text = ctx.Boolean().getText();
            if (text.equals("true"))
                return new BoolLiteral(true);
            else if (text.equals("false"))
                return new BoolLiteral(false);
            else throw new RuntimeException("Unexpected program state in ExpressionVisitor!");
        }

        @Override
        public Expression visitIntegerLiteral(MashParser.IntegerLiteralContext ctx) {
            int value = Integer.parseInt(ctx.Integer().getText());
            return new IntegerLiteral(value);
        }
        @Override
        public Expression visitStringLiteral(MashParser.StringLiteralContext ctx) {
            String value = ctx.String().getText();
            value = value.substring(1, value.length()-1);
            return new StringLiteral(value);
        }

        @Override
        public Expression visitVariable(MashParser.VariableContext ctx) {
            return new Variable(ctx.Identifier().getText());
        }

        @Override
        public Expression visitMushVariable(MashParser.MushVariableContext ctx) {
            return new Variable(ctx.Identifier().getText());
        }

        @Override
        public Expression visitParenthesis(MashParser.ParenthesisContext ctx) {
            return visit(ctx.expression());
        }

        @Override
        public Expression visitFunctionCall(MashParser.FunctionCallContext ctx) {
            String functionName = ctx.Identifier().getText();

            List<Expression> arguments =  new ArrayList<>();
            for (MashParser.ExpressionContext argumentCtx : ctx.expression()) {
                arguments.add(visit(argumentCtx));
            }

            return new FunctionCall(functionName, arguments);
        }

        @Override
        public Expression visitUnaryMinus(MashParser.UnaryMinusContext ctx) {
            Expression argument = visit(ctx.factorExpression());
            return new FunctionCall("-", argument);
        }

        @Override
        public Expression visitBinaryTerm(MashParser.BinaryTermContext ctx) {
            return binaryOperation(ctx);
        }

        @Override
        public Expression visitBinarySum(MashParser.BinarySumContext ctx) {
            return binaryOperation(ctx);
        }

        @Override
        public Expression visitBinaryCompare(MashParser.BinaryCompareContext ctx) {
            return binaryOperation(ctx);
        }

        private Expression binaryOperation(ParserRuleContext ctx) {
            Expression leftArg = visit(ctx.getChild(0));
            String operator = ctx.getChild(1).getText();
            Expression rightArg = visit(ctx.getChild(2));
            return new FunctionCall(operator, leftArg, rightArg);
        }

        @Override
        public Expression visitMush(MashParser.MushContext ctx) {
            String sisu = ctx.getText();
            sisu = sisu.substring(1, sisu.length()-1);
            if (sisu.length() == 0)
                return new EmptyMush();

            String[] elements = sisu.split(",");
            if (elements[0].startsWith("\"")) {
                List<String> values = new ArrayList<>(Arrays.asList(elements));
                return new StringMush(values);
            } else {
                List<Integer> values = new ArrayList<>();
                for (String e : elements) {
                    try {
                        values.add(Integer.parseInt(e.strip()));
                    } catch (NumberFormatException exception) {
                        throw new RuntimeException(
                                "Incompatible types for elements in mush initialization: "
                                        + Arrays.toString(elements));
                    }
                }
                return new IntMush(values);
            }
        }

        @Override
        public Expression visitBoolBinOp(MashParser.BoolBinOpContext ctx) {
            String op = ctx.op.getText();
            Expression left = visit(ctx.left);
            Expression right = visit(ctx.right);
            return new BoolBinOp(op, left, right);
        }

        @Override
        public Expression visitBinaryMushSum(MashParser.BinaryMushSumContext ctx) {
            String functionName = ctx.op.getText();

            List<Expression> arguments =  new ArrayList<>();
            arguments.add(visit(ctx.left));
            arguments.add(visit(ctx.right));

            return new MushFunction(functionName, arguments);
        }


    }

    private static class MashAstStatementVisitor extends MashBaseVisitor<Statement> {
        private final MashAstExpressionVisitor expressionVisitor = new MashAstExpressionVisitor();

        @Override
        public Statement visitExpression(MashParser.ExpressionContext ctx) {
            Expression expression = expressionVisitor.visit(ctx);
            return new ExpressionStatement(expression);
        }

        @Override
        public Statement visitMushAdd(MashParser.MushAddContext ctx) {
            Expression expression = expressionVisitor.visit(ctx.expression());
            return new MushAdd(ctx.Identifier().getText(), expression);
        }

        @Override
        public Statement visitPrintStatement(MashParser.PrintStatementContext ctx) {
            MashParser.ExpressionContext exprToPrint = ctx.expression();
            Expression expression = null;
            if (exprToPrint != null)
                expression = expressionVisitor.visit(exprToPrint);
            return new MushPrint(expression);
        }

        @Override
        public Statement visitVariableDeclaration(MashParser.VariableDeclarationContext ctx) {
            String variableName = ctx.VariableName.getText();
            Expression initializer = null;
            if (ctx.expression() != null)
                initializer = expressionVisitor.visit(ctx.expression());
            return new Declaration(variableName, initializer);
        }

        @Override
        public Statement visitAssignStatement(MashParser.AssignStatementContext ctx) {
            String variableName = ctx.Identifier().getText();
            Expression expression = expressionVisitor.visit(ctx.expression());
            return new Assignment(variableName, expression);
        }

        @Override
        public Statement visitStatements(MashParser.StatementsContext ctx) {
            List<Statement> statements = new ArrayList<>();
            for (MashParser.StatementContext statementContext : ctx.statement()) {
                statements.add(visit(statementContext));
            }
            return new Block(statements);
        }

        @Override
        public Statement visitProgram(MashParser.ProgramContext ctx) {
            return visit(ctx.statements());
        }
    }
}